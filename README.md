encb
====
An encrypted backup solution.

More details are available [here](http://thoughts.z-dev.org/2013/11/05/designing-a-secure-backup-solution/).
