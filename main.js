

// // credential testing
// var CredentialsManager = require('./lib/CredentialsManager');
// cmInstance = new CredentialsManager('test');


global.IGNORE_DOT_FILES = true;


var fs     = require('fs'),
    util   = require('util'),
    events = require('events');

// fs.watch(__dirname, function(ev, fName) {
// 	console.log(fName, ev);
// });


var DirectoryWatcher = function() {
	events.EventEmitter.call(this);
	this.directoryTree = {};
};

util.inherits(DirectoryWatcher, events.EventEmitter);

// DirectoryWatcher.prototype.constructor = function() {
	
// };	

DirectoryWatcher.prototype.listen = function(baseDirectory, callbackFunction) {

	var listenFunction = function(parentDirectory, fileName) {

		// ignore dot files?
		if(global.IGNORE_DOT_FILES && fileName.substring(0,1) == '.') return;

		// here we check to see if we are currently listening to the
		// changed file. If not, then we attempt to listen
		if(!this.directoryTree[parentDirectory + '/' + fileName])
			this._watch(parentDirectory + '/' + fileName, listenFunction);

		// get parent directory information
		var cachedParentContents = this.directoryTree[parentDirectory];

		// current directory contents
		this._getDirectoryContents(parentDirectory, function(err, currentParentContents) {

			// TODO: use event emitter to fire FileEvents which describe the cwd, file, and type
			/*
				Notes:
				------
				If a directory is renamed, we must invalidate all the children or
				we may diverge. Unless..


				Give each file a unique element id. Then you have a parent-child relationship
				enabling you to quickly change meta-data about each of the elements but you
				then do not have to invalidate all of the children.

				Hmmm.. example:

				/ => [123]
				/test.js => [456]


				then, we have refernces stored as:
				[123] <=> [456]
				where [123] := [/]
				and   [456] := [test.js]

				If we follow all of the links, we get:

				[start] => [123]  => [456]
							'/'   +  'test.js'  :=> "/test.js"

				If we rename the file 'test.js' to 'old.js', we update the node:
				[456] := [old.js]
				But the refernce stays the same implying:
				[123] => [456] :=> '/' + 'old.js'
				



				// potential object to store this data
				{
					'123' : {
						file        : '/',
						isDirectory : true,
						children    : [ '456' ],
						parent      : null
					},

					'456' : {
						file        : 'test.js',
						isDirectory : false,
						parent      : '123'
					}

				}


				Admittedly, traversing directories or searching would be slow. And how do you identify
				blocks from files? Traversing the tree.

				Your initial lookup has O(N) complexity, and there after, you have 'm' parent directories,
				so, you get:

					O(N) + sigma[0-n; i]( O(len(N-i)) )


				This is because:
					1: you must find the parent-most element
					2. you have to find the child element from the parent that matches
					   the next directory step (ie: / => /test/)
					3. if that has children, you must iterate across them too, matching
					   the next child.
					4. this continues until it is matched

				It would be interesting to implement, none the less. A rename, however becomes trivial then.

			*/

			// check for type of change
			if(cachedParentContents.length < currentParentContents.length) {
				console.log('new file:', fileName);
			} else if(currentParentContents.length < cachedParentContents.length) {
				console.log('file deleted:', fileName);
			} else {
				console.log('file modified', fileName);
			}

			// place file listing into directory tree cache
			this.directoryTree[parentDirectory] = currentParentContents;

		}.bind(this));

		console.log('Change:', parentDirectory, '=>', fileName);
		this.emit('wat', 'wat');

	}.bind(this);

	// fire initial watch command on provided directory
	this._watch(baseDirectory, listenFunction);

	// return current instance for stacked references
	return this;

};

DirectoryWatcher.prototype._watch = function(fileName, callbackFunction) {

	// stat passed file
	fs.stat(fileName, function(err, stats) {

		// no errors running stat and file is directory
		if(!err && stats && stats.isDirectory()) {

			// set up file-system watch
			fs.watch(fileName, function(ev, fileName) {

				// fire parent-most callback function
				this[0](this[1], fileName);

			}.bind([callbackFunction, fileName]));
		
			// read all files
			fs.readdir(fileName, function(err, fileList) {

				// cache file list
				this.directoryTree[fileName] = fileList;

				fileList.forEach(function(aFile) {

					// watch file
					this._watch(fileName + '/' + aFile, callbackFunction);

				}.bind(this));

			}.bind(this));

		}

	}.bind(this));

};

DirectoryWatcher.prototype._getDirectoryContents = function(directory, callbackFunction) {
	fs.readdir(directory, callbackFunction);
};




// testing
var dw = new DirectoryWatcher();

dw.listen(__dirname).on('wat', function(a) {
	console.log('wat:', a);
})