encb tasks
==========
This is a list that I will be following and adding to in order to get encb to a usable state. The list is divide based upon a task or a group of tasks that are related.


file system
--
* recursive file and folder listening
    * support for event types (change, rename, deletion)
* file tree generation

file table
--
* initialization
* storage
* retrieval
* block lookups (for deduplication)

encryption
--
* key generation
* file hashing
* file segmenting
    * block hashing
    * block encryption
    * ordered listing of all blocks (async?)

remote storage
--
* authentication (eventually)
* block upload
* block retrieval

interface
--
* important or not to offer an interface?
* use node-qt to render?


other
--
* [binary compilation?](https://github.com/crcn/nexe)
