var FileEvent = function() {
	this.constructor.apply(this, arguments);
};

FileEvent.prototype.constructor = function(parentDirectory, fileName, eventType) {
	this.parentDirectory = parentDirectory;
	this.fileName        = fileName;
	this.eventType       = eventType;
};

