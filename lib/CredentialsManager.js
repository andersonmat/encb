var crypto = require('crypto');

var CredentialsManager = function() {
	this.constructor.apply(this, arguments);
};

CredentialsManager.prototype.constructor = function(keySeed) {

	this._generatePrivateKey(keySeed);

};


CredentialsManager.prototype._generatePrivateKey = function(keySeed) {

	// use pbkdf2 to generate a key suitable for AES
	// of 256 length and from 150000 iterations
	crypto.pbkdf2(keySeed, this._getSalt(keySeed), 150000, 256, function(err, dKey) {

		if(!err) this.pKey = dKey;

	}.bind(this));

};

CredentialsManager.prototype._getSalt = function(keySeed) {
	var shaHash = crypto.createHash('sha512');
	shaHash.update(keySeed, 'utf8');
	return shaHash.digest();
};



module.exports = CredentialsManager;