

var crypto = require('crypto'),
    fs     = require('fs');



// var dhInstance = crypto.createDiffieHellman(64); // fucking change this
// var publicKey = dhInstance.generateKeys('base64');


// console.log(
// 	'Private Key:', dhInstance.getPrivateKey('hex')
// );

var privateKey = "abcdefghijklmnopqrstuvwxyz012345"; // this will be replaced by DH generation


var splitLength = 1024; // 1 kilobyte

var frStream = fs.createReadStream(__dirname + '/test.txt');

frStream.on('open', function(fd) {
	doFileFragment(fd, 0, 0);
});



var doFileFragment = function(fileDescriptor, fileOffset, bIndex) {

	//console.log('fd:', fileDescriptor, 'offset:', fileOffset);

	fs.read(fileDescriptor, new Buffer(splitLength), 0, splitLength, fileOffset, function(err, bRead, fData) {

		if(!err && bRead && fData) {

			// prepend length of data
			var dLenBuffer = new Buffer(2);
			dLenBuffer.writeUInt16LE(bRead, 0);
			fData = Buffer.concat([dLenBuffer, fData]);

			// create new cipher instance
			var cipherInstance =
				crypto.createCipher(
					'aes256',
					privateKey
				);

			// add file data to cipher
			var encipheredData = cipherInstance.update(fData);
			var finalizedBuffer = Buffer.concat([
				encipheredData,
				cipherInstance.final()
			]);

			// create a hash of enciphered data
			var blockHash = crypto.createHash('sha512');
			blockHash.update(finalizedBuffer);
			var blockDigest = blockHash.digest('hex');

			console.log('block', bIndex + ':', blockDigest);



			// testing here
			var decipherInstance = crypto.createDecipher('aes256', privateKey);
			var partialContents = decipherInstance.update(finalizedBuffer);
			var completeContents = Buffer.concat([
				partialContents, decipherInstance.final()
			]);

			// read data length
			var dataLength = completeContents.readUInt16LE(0);
			console.log('data length:', dataLength);

			var finalData = completeContents.slice(2, 2 + dataLength); // get contents continuing after length
			console.log('Contents:', finalData.toString('utf8'));





			// must we do more?
			if(bRead == splitLength) {
				setTimeout(function(){
					doFileFragment(fileDescriptor, fileOffset + bRead, ++bIndex);
				}, 0);
			}


		//	console.log('enc data:', finalizedBuffer.toString('base64'));


		}

	})

};




